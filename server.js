const path = require('path'),
      express = require('express');

const app = express();

app.use(express.static('public'));

app.use('/js', express.static(path.join(__dirname, '/public/js')));
app.use('/css', express.static(path.join(__dirname, '/public/css')));
app.use('/img', express.static(path.join(__dirname, '/public/img')));

const server = app.listen(8080, () => {
    let PORT = server.address().port;
    console.log(`Server started on port ${ PORT }`);
})