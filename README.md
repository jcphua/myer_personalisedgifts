# Myer _Personalised Gifts_ static 'microsite'

Simple static microsite developed for Myer's 2015 Christmas *Personalised Gifts* campaign.

Site has been developed by Taguchi with a *shopping-cart experience* to allow customers to select and personalise their products being ordered. 

Upon submission of their order, the customer's data is collated and forwarded via email to internal Myer stakeholders for further manual processing/product fulfilment.

The 'shopping-cart' has no underlying client or server requirements (ie. No dependency on cookies, client-JS-storage, or server-storage), so can be hosted as a self-contained simple static website requiring only JS and CSS.



## DEVELOPER NOTES

Site has been rapidly developed to allow for efficient customisation of product data.

Core files:

| Filename | Details
|---|---|
| `index.html` | Site default container and placeholder content
| `js/script.js` | Site initialisation script
| `js/jquery.taguchi.choices.js` | Site interactivity script
| `js/choices.json` | Product configuration data
| `js/debug.js` | Debug aide: pre-populate cart with defined test data



## BUILD DEPENDENCIES

### \*\* Sass pre-compiler required \*\*

#### Install Sass

##### OSX

**RUBY REQUIRED TO INSTALL SASS**, but is pre-installed on OSX.

(Source: <http://coolestguidesontheplanet.com/running-installing-sass-osx/>)

1. `sudo gem install sass`
2. Install dependency `sudo gem install --version '~> 0.9' rb-fsevent`

##### Windows

1. `yarn add node-sass` or npm i node-sass

#### Auto-compile Sass

##### OSX

`sass --watch {input-dir}:{output-dir}`

eg. `sass --watch src/sass:public/css`

##### Windows

1. `.\node_modules\.bin\node-sass --watch ./src/sass --output ./public/css`



### Sass manual
<http://sass-lang.com/guide>

