var cartData = [
   {
      "quantity":1,
      "user_choice":[
         {
            "name":"item_type",
            "field_name":"item_type",
            "user_option_index":0
         },
         {
            "name":"personalisation",
            "field_name":"nutella_personalisation",
            "value":"asdf"
         }
      ]
   },
   {
      "quantity":3,
      "user_choice":[
         {
            "name":"item_type",
            "field_name":"item_type",
            "user_option_index":1
         },
         {
            "name":"character",
            "field_name":"mrmen_character",
            "user_option_index":7
         },
         {
            "name":"frame",
            "field_name":"mrmen_frame",
            "inset_name": "size",
            "user_option_index":0,
            "user_option_inset_index":1
         },
         {
            "name":"personalisation",
            "field_name":"mrmen_personalisation",
            "value":"sdfs"
         }
      ]
   },
   {
      "quantity":1,
      "user_choice":[
         {
            "name":"item_type",
            "field_name":"item_type",
            "user_option_index":2
         },
         {
            "name":"type",
            "field_name":"mensleather_type",
            "user_option_index":1
         },
         {
            "name":"item",
            "field_name":"mensleather_item",
            "user_option_index":0
         },
         {
            "name":"personalisation",
            "field_name":"mensleather_personalisation",
            "value":"sdf"
         }
      ]
   },
   {
      "quantity":2,
      "user_choice":[
         {
            "name":"item_type",
            "field_name":"item_type",
            "user_option_index":3
         },
         {
            "name":"type",
            "field_name":"santasack_type",
            "user_option_index":0
         },
         {
            "name":"color",
            "field_name":"santasack_color",
            "user_option_index":1
         },
         {
            "name":"personalisation",
            "field_name":"santasack_personalisation",
            "value":"sdfs"
         }
      ]
   }
];

$(function() {
    $('html').on({
        'debug-customer': function(e) {
            $('[name=subscriber_firstname]').val(arguments[1] || 'j');
            $('[name=subscriber_lastname]').val(arguments[2] || 'p');
            $('[name=email]').val(arguments[3] || 'jaycee.phua+tests@taguchimail.com');
            $('[name=subscriber_phone]').val(arguments[4] || '03 9290 4700');
            $('[name=subscriber_custom_MYER_one_number]').val(arguments[5] || '123456');
            if (true) {
                $('[name=subscriber_address]').val(arguments[6] || 'Level 4, 499 St Kilda Rd');
                $('[name=subscriber_suburb]').val(arguments[7] || 'Melbourne');
                $('[name=subscriber_state]').val(arguments[8] || 'VIC');
                $('[name=subscriber_postcode]').val(arguments[9] || '3004');
            }
            $('#agree-terms').prop("checked", true || arguments.length > 10);
            
            
            if ($('#pnl-address_delivery').is(':visible')) { //$('[name=subscriber_custom_address_delivery_different]').is(':checked')) {
                $('[name=subscriber_custom_address_delivery_different]').prop("checked", true || arguments.length > 11);
                $('[name=subscriber_custom_delivery_firstname]').val(arguments[12] || 'J');
                $('[name=subscriber_custom_delivery_lastname]').val(arguments[13] || 'P');
                $('[name=subscriber_custom_delivery_address]').val(arguments[14] || 'LEVEL 4, 499 ST KILDA RD');
                $('[name=subscriber_custom_delivery_suburb]').val(arguments[15] || 'MELBOURNE');
                $('[name=subscriber_custom_delivery_state]').val(arguments[16] || 'VIC');
                $('[name=subscriber_custom_delivery_postcode]').val(arguments[17] || '3004');
            }
            
            
        },
        'debug-cart': function (e) {
            var $journey = $("#journey");
            $journey.choices("cart", cartData);
            
            $('.block-santa').on('dblclick', function() {
                $('html').triggerHandler('debug-customer');
            })
            return $journey.data("plugin_choices").cart;
        },
        'hash-detect': function (e) {
        	var _hash = location.hash.substr(1);
            $(window).on('load', function(e) { // 'hashchange' doesn't work yet
            	if (_hash.length) {
                    switch (_hash) {
                        case "cart":
                            setTimeout(function() {
                                $('html').triggerHandler('debug-cart');
                                $("#journey").choices('page', 'cart');
                            }, 1000);
                            break;
                        default:
                            $('#journey').choices('page', _hash);
                            break;
                    }
            		
            	}
            });
        }
    });

    $('html').trigger('hash-detect');
    
    
    $('form').append($('<input />').attr({
        'type': 'hidden',
        'name': 'annotation_test_mode',
        'data-submit-keep': ''
    }).val('true'));
    $('#journey').choices('submit', false);
});
