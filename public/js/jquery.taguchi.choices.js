;(function($, window, document, undefined) {
	var pluginName = "choices",
        version = "0.1";
	var defaults = {
		"dataName": pluginName,
		"dataSource": "choices.json?1",
        "pages": {
            "entry": "#start",
            "home": "#home",
            "exit": "#continue",
            "cart": "#cart"
        },
		/*
        "triggers": {
			"tile-input": function(e) {}
			"animate-start": function(e) {}
			"animate-step": function(e) {}
			"animate-complete": function(e) {}
		},
        */
		"animate": "anim-vert",
		"homeName": "choice",
        "imagePath": "./img"
	};
    
	function Plugin(element, options) {
		var _plugin = this;
        
		this.__name = pluginName;
		this.element = element;
		this.__defaults = defaults;
		this.options = $.extend(true, {}, defaults, options);
		this.debug = this.options.debug || false;
		this.allowSubmit = this.options.allowSubmit || true;

		this._data;
        this._user_choice;
        
        this.$page_current = null;
        this.$page_next = null;
        this.$root = null;
        
		_plugin.init();
	}
    
	Plugin.prototype = {
		"cart": function () {
			var _plugin = this;

			if (!arguments.length) { return _plugin.cart; }
			_plugin.cart = arguments[0];
			_plugin.setNavCart();
		},

        "page": function() {
            var _plugin = this;
            
            if (!arguments.length) { return; }
            var arg = (/^(#)/.test(arguments[0]))? arguments[0].substr(1) : arguments[0],
            	redirect = {
            		"cart": "view-order",
					"start": "entry"
            	};

			if (/^(cart|details|entry|home|start)$/.test(arg)) {
                _plugin.setPage((redirect[arg])? redirect[arg] : arg);
            }
        },
        
        "reset": function () {
        	var _plugin = this;

        	_plugin.resetAll();
        },

        "submit": function (state) {
        	var _plugin = this;

        	_plugin.allowSubmit = state;
        },

        init: function () {
			var _plugin = this;
            
			_plugin.initReferences();

			_plugin.loadData(function () {
				_plugin.initHomeTiles();
				_plugin.initPages();

				$("section[data-choices-ref='entry']", _plugin.$root).nextAll().addClass("unseen");

				_plugin.initControls();
				if (_plugin.debug) { console.log(_plugin); }
			});
            
		},

		loadData: function(callback) {
			var _plugin = this;
			var _dest = (arguments.length > 1) ? {} : _plugin;

			$.getJSON(_plugin.options.dataSource, function (_data) {
				if (!_data.choices) { console.error("[Choices]: Invalid data source - expected 'choices' property."); return; }
				if (_dest != _plugin) { _dest = _data; }
				else {
					if (_data.name && _plugin.options.dataName != _data.name) { _plugin.options.dataName = _data.name; }
					_plugin.data = $.extend({}, _data);
				}
				return callback(_dest);
			}).fail(function() {
			    console.error('[Choices]: Data load error - ', arguments);
			});
		},
        
		initReferences: function() {
			var _plugin = this;
			var $plugin = $(_plugin.element),
				_options = _plugin.options;

			if (/^(horz|vert)$/.test(_options.animate)) { $plugin.addClass('anim-' + _options.animate); }

			if (!$plugin.find("[data-choices-ref='root']").length) {
				_plugin.$root = $(_plugin.element);
				$(_plugin.$root).attr("data-choices-ref", "root");
			}
			else {
				_plugin.$root = $plugin.find("[data-choices-ref='root']");
			}
            _plugin.$page_current = $("section:visible");
			var $root = _plugin.$root;
			if (_options.pages) {
				$.each(_options.pages, function (i, x) {
					$root.find(x).attr("data-choices-ref", i);
				});
			}
		},

		initHomeTiles: function() {
			var _plugin = this;
			var $home = $(_plugin.$root);
			var $tilesRoot = $('.tiles', $home),
                $navMenuList = $('header #nav-menu ul');
            
			if ($tilesRoot.children('li').length) { return; }
            
			$.each(_plugin.data.choices, function(i, x) {
				var _data = $.extend(true, x, { 
					"name": _plugin.options.homeName, 
					"value": x.name,
                    "style": "master"
				});
				var $tile = _plugin.markupListItem(_data);
				$tilesRoot.append($tile);
                $navMenuList.append($('<li />').attr('data-choices-preset', '').append($('<a />').attr('href', '#' + x.value).html(x.nav_markup? x.nav_markup : x.summary_markup? x.summary_markup : x.label)));
			});
            $home.find("fieldset").data({
                'choices_data': _plugin.data, 
                'choices_index': 0 
            });
		},
        
		initPages: function() {
			var _plugin = this;
			if (_plugin.debug) { console.group("[Choices]"); }
			$.each(_plugin.data.choices, function (i, x) {
				// if (_plugin.debug) { console.log(i, x, x.sections); }
				if (!x.sections) { return true; }
				var choiceName = x.value;
				var $choice = $("<section />").attr('data-choices-path', choiceName),
					$div = $('<div />'),
					$wrapper = $div.clone().addClass('wrapper'),
					$container = $div.clone().addClass('container'),
					$content = $div.clone().addClass('content');

                if (_plugin.debug) { console.group(i, x.value, x); }
                var ctr;
                $.each(x.sections, function (j, y) {
                	var $step = _plugin.markupStep(y, choiceName, 0);
                	if (j > 0) { $step.addClass('disabled'); }
                    if ($step.length > 1) { 
                        var ref;
                        ctr = 0;
                        $.each($step, function(k, z) { 
                            if ($(z).attr("data-choices-ref") !== ref) { 
                                ref = $(z).attr("data-choices-ref"); 
                                ctr++;
                            }
                            $(z).data("choices_index", ctr);
                            if (ctr > 1) { $(z).hide(); } // REMOVE FOR ALWAYS-VISIBLE STEPS ON PAGE
                        }); 
                    }
                    else { 
                        if (!ctr) { ctr = j; }
                        ctr++;
                        $step.data("choices_index", ctr);
                        if (ctr > 1) { $step.hide(); } // REMOVE FOR ALWAYS-VISIBLE STEPS ON PAGE
                    }
					$content.append($step);
				});
				if (_plugin.debug) { console.groupEnd(); }

				$choice.append($wrapper.append($container.append($content)));

				$choice.insertBefore($(_plugin.options.pages.exit, _plugin.element));
			});
			if (_plugin.debug) { console.groupEnd(); }
		},
        
        initControls: function () {
        	var _plugin = this;
            var _userTriggers = _plugin.options.triggers || [];

        	$("fieldset, .control a.btn").each(function (i, x) {
        		if (!$(x).is("[class]")) { return true; }
        		$(x).attr("data-choices-preset", "").data("choices-preset", $(x).attr("class"));
        	});

        	$("section[data-choices-path] ul.fields input.text", $(_plugin.element)).on('keyup', $.debounce(100, function (e) {
        		var $src = $(e.currentTarget),
					$btn = $src.closest('section').find('.btn');

        		if ($src.val().length) {
                    $src.closest("li.assist").removeClass("assist");
        			$btn.removeClass('disabled');
        		}
        		else if (!$btn.hasClass('disabled')) {
        			$btn.addClass('disabled');
        		}
        	}));

        	$("input:checkbox, input:radio", $("ul.tiles", $(_plugin.element))).on({
        		'change': function (e) {
        			var $src = $(e.currentTarget),
                        $page_curr = $(_plugin.$page_current);
        			var $step_curr = $src.closest("fieldset"),
						isRequired = $page_curr.find('ul.tiles').hasClass("input-required"),
                        $control = $page_curr.find('.control');
        			var $step_next = $step_curr.next('fieldset');

                    _plugin.setUserChoice($src);
        			_plugin.toNextStepFX($src);

        			var _inputs = [];
        			if ($('fieldset', $page_curr).length > 1) {
        				$('fieldset:visible input', $page_curr).each(function (i, x) {
        					if ($.inArray($(x).attr("name"), _inputs) < 0) { _inputs.push($(x).attr("name")); }
        					// _inputs.push("input[name='" + $(x).attr("name") + "']");
        				});
        			}
        			var _checkedInputs = $.map(_inputs, function(x, i) {
        				return "input[name='" + x + "']:checked";
        			});
        			
        			if ($(_checkedInputs.join(', '), $page_curr).length === _inputs.length) { $control.find('.btn.disabled').removeClass('disabled'); }

        			if (false && _userTriggers && _userTriggers['tile-input']) {
        				_userTriggers['tile-input'](e);
        			}
        			else {
        				if ($src.is("[name='mrmen_character']")) {
        					var selected = $src.val();
        					$("ul.tiles > li", $step_next).each(function (i, x) {
        						var $img = $(x).find("img");
        						var _src = $img.attr("src").split("_");
        						$img.attr("src", [_src[0], selected, _src[2]].join("_"));
        					});
        				}
                        else if ($src.is("[name='santasack_type']")) {
                            var selected = $src.val();
                            $("ul.tiles > li", $step_next).each(function(i, x) {
                                var $img = $(x).find("img"),
                                    $input = $(x).find("input");
                                var _src = $img.attr("src").split("_");
                                $img.attr("src", [_src[0], _src[1], selected, $input.val() + ".jpg"].join("_"));
                            });
                        }
        			}
        		}
        	});

        	$("section .control a.btn:not([target])", $(_plugin.element)).on({
        		'click': function (e) {
        			e.preventDefault();
        			var $src = $(e.currentTarget);
        			var srcHref = $src.attr('href');
                    if (/^(http)/.test(srcHref)) { 
                        document.location.href= srcHref;
                        return; 
                    }
        			if ($src.hasClass('disabled')) { return; }

        			_plugin.$page_current = $src.closest("section");
                    
                    $("li.assist", _plugin.$page_current).removeClass("assist");
                    _plugin.setUserChoice($src);

        			if (/^(#)/.test(srcHref)) {
   						_plugin.setPage(srcHref.substr(1));
        			}
        		}
        	});
            
            $("[data-choices-ref='root']").on({
                'mouseover': function(e) {
                    var $el = $(e.currentTarget);
                    var $curr_page = $el.closest('section');
                    if (!$("[name]:empty", $curr_page).length) { return; }
                    $("[name]:empty", $curr_page).each(function(i, x) {
                        $(x).closest('li').addClass('assist');
                    });
                },
                'mouseout': function(e) {
                    var $el = $(e.currentTarget);
                    var $curr_page = $el.closest('section');
                    if (!$("[name]:empty", $curr_page).length) { return; }
                    $("[name]:empty", $curr_page).each(function(i, x) {
                        $(x).closest('li').removeClass('assist');
                    });
                }
            }, 'section .control a.btn.disabled');
            
            $('section#details .control a.btn', $(_plugin.element)).on({
                'mouseover': function(e) {
                    var $el = $(e.currentTarget);
                    var $curr_page = $el.closest('section');
                    if (!$(".error", $curr_page).length) { return; }
                    $el.addClass('disallow');
                    $(".error", $curr_page).each(function(i, x) {
                        var $listItem = $(x).closest('li');
                        $listItem.addClass('assist');
                    });
                },
                'mouseout': function(e) {
                    var $el = $(e.currentTarget);
                    var $curr_page = $el.closest('section');
                    $('.disallow', $el).removeClass('disallow');
                    $('li.assist', $curr_page).removeClass('assist');
                }
            })

            $('ul.tiles label img').on('click', function (e) {
            	if (!$('html').hasClass('ie11')) { return; }
            	$(e.currentTarget).closest('label').find('input').trigger('click');
            });
            
            _plugin._timer;
            $(window).on('resize', function() {
                clearTimeout(_plugin.timer);
                _plugin.timer = setTimeout(function() {
                    if (!$(_plugin.$page_current).find('ul.tiles li[data-height]').length) { return; }
                    _plugin.setOptionHeights(_plugin.$page_current);
                }, 250);
            });
        },

		markupStep: function(_data, choiceName, recurLevel) {
			var _plugin = this;

			if (_plugin.debug) { console.log('[' + recurLevel + ']', choiceName, _data.sections != null? "sections" : _data.options != null? "options" : "", _data.content, _data); }

			var $step = $('<fieldset />'),
				$div = $('<div/>'),
				$header = $div.clone().addClass('header'),
				$panel = $div.clone().addClass('panel'),
				$tiles = $('<ul />').addClass(_data.listing_class || "tiles"),
				headingText, footnoteText, controlButtons,
				$intro,
				option_value, option_main_name;

			if (recurLevel > 0) {
				option_value = arguments[3],
				option_main_name = arguments[4];
				$step.addClass('disabled').hide();  // REMOVE .hide() FOR VISIBLE STEPS ON PAGE
			}

			if (_data.content) {
				headingText = (_data.content.header && _data.content.header.title) ? _data.content.header.title : "[Missing heading]";
				footnoteText = (_data.content.footnote) ? _data.content.footnote.text : null;
				var $heading = $('<h1 />').append(headingText);
				if (_data.content.header) {
					if (_data.content.header.image) { }
					if (_data.content.header.icon_class) { $heading.prepend($('<i />').addClass(_data.content.header.icon_class)); }
				}
				$header.append($heading);

				if (_data.content.intro) {
					var _intro = _data.content.intro;
					if (typeof (_intro) === "object") {
						$intro = $(_intro.html_markup);
					}
					else {
						$intro = $('<p />').html(_intro);
					}
					$panel.append($intro);
				}

				if (_data.content.footnote) { footnoteText = _data.content.footnote; }
				if (_data.content.control) { controlButtons = _data.content.control; }
			}

			var _pagesExtra = [];
			if (_data.options) {
				var option_name = _data.name;
				if (_plugin.debug) { console.log('[' + recurLevel + ']', choiceName, _data.name, _data.options.length, _data.options); }
				if (recurLevel > 0) { $step.attr('data-choices-show-condition', [[choiceName, option_main_name].join('_'), option_value].join('=')); }
				else {
					option_main_name = _data.name;
				}
				$.each(_data.options, function (i, x) {
					var x_choiceName = [choiceName, (recurLevel > 0)? option_value : option_name].join('_');
					if (_plugin.debug) { console.log('[' + recurLevel + ']', i, choiceName, option_name, option_value, ':', x_choiceName, x.value, x); }
					if (x.sections) {
						if (_plugin.debug) { console.groupCollapsed("sections", i + ':', x.value, x.sections.length); }
						//if (x.sections.length > 1) { x_choiceName = [choiceName, option_name].join('_'); }
						$.each(x.sections, function (j, y) {
							_pagesExtra.push(_plugin.markupStep(y, choiceName, recurLevel + 1, x.value, (recurLevel === 0? [option_main_name].join('_') : null)));
						});
						if (_plugin.debug) { console.groupEnd(); }
					}
					var _x_data = $.extend(true, x, {
						"name": x_choiceName,
						"parent_listing_class": _data.listing_class,
						"parent_input_type": _data.input_type,
						"recurLevel": recurLevel
					});
                    if (_data.style) { _x_data.style = _data.style; }
					if (_data.mode && _data.content[_data.mode]) {
						_x_data['parent_mode'] = _data.mode;
						_x_data['mode_content'] = _data.content[_data.mode];
					}
					var $tile = _plugin.markupListItem(_x_data);
					$tiles.append($tile);
                    
				});
				var optionsLength = _data.options.length;
				if (optionsLength % 4 === 0 || optionsLength > 8) { $tiles.addClass('fit-four'); }
				else if ((optionsLength % 3 === 0 || optionsLength % 5 === 0) && optionsLength <= 6) { $tiles.addClass('fit-three'); }
				else if (optionsLength % 2 === 0 && optionsLength <= 4) { $tiles.addClass('fit-two'); }
				if (_data.input_type === 'radio' || _data.required) { $tiles.addClass('input-required'); }
				$panel.append($tiles);
			}
			$step.append($header);

			var $pad = $div.clone().addClass('pad');
			$pad.append($panel);
			if (controlButtons) {
				var $control = $div.clone().addClass('control');
				$.each(controlButtons, function (i, x) {
					var $button = $("<a />").attr("href", x.href).addClass(['btn', x.class].join(' ')).text(x.label);
					$control.append($button);
				});

				$pad.append($control);
			}

			if (footnoteText) { $pad.append($('<div />').addClass('footnote').html(footnoteText)); }
			$step.append($pad);

            $step.attr("data-choices-ref", [choiceName, _data.name].join('_')).data('choices_data', _data);

			if (_pagesExtra.length) {
				_pagesExtra.unshift($step);
				$step = _pagesExtra;
			}

			return $step;
		},
        
		markupListItem: function(_data) {
			var _plugin = this,
				listType = _data.parent_listing_class;
			var $tile = $("<li />"),
                $label = $("<label />"),
                $image = $("<img />"),
                $span = $("<span />"),
                $input = $("<input />"),
                $div = $("<div />");

			var placeholderText, maxLength
			
			$image.attr("src", _data.image);
			if (_data.inputClass) { $input.addClass(_data.inputClass); }
			if (_data.parent_mode) {
				if (_data.parent_mode === "personalisation") {
					placeholderText = ['Enter ', _data.mode_content.inputType, ' (', _data.mode_content.maxLength, ' character limit)'].join('');
					maxLength = _data.mode_content.maxLength;
				}
			}
            
            if (_data.style) { $label.addClass(_data.style); }
            
			if (listType === "fields") {
				var label, title;

				placeholderText = placeholderText || _data.input_placeholder;

				$input.attr({
					"type": _data.input_type || "text",
					"name": _data.name,
					"placeholder": placeholderText,
					"title": _data.input_title
				}).addClass(_data.input_class);
				if (_data.value) { $input.val(_data.value); }
				if (maxLength) { $input.attr('maxlength', maxLength); }

				$label.append((_data.summary_markup) ? _data.summary_markup : _data.label).addClass('modal');

				$tile.append($div.clone().addClass('input-wrapper').append($label, $input));
			}
			else if (listType === "tiles" && _data.options_inset) {
				var $div_inset = $div.clone().addClass('inset-choice'),
					$optionList = $('<ul />');
				$.each(_data.options_inset, function (i, x) {
					var $option = $('<li />'),
						$x_input = $input.clone(),
						$x_label = $label.clone();
					$x_input.attr({
						"type": "radio",
						"name": _data.name,
						"value": [_data.value, x.value].join('_'),
					}).addClass(_data.parent_input_type);
					var x_label = x.summary_markup ? x.summary_markup : x.label,
						x_title = [_data.label, x.label];

					if (x.brand) { x_title.unshift(x.brand); }
					
					$x_label.append($x_input, $('<span />').html(x_label)).attr('title', x_title.join(' '));
					$option.append($x_label);

					$optionList.append($option);
				});
				$div_inset.append($optionList);

				$div.append($image);
				$div.append($span.append(_data.summary_markup? _data.summary_markup : _data.label));
				$div.append($div_inset);

				$tile.append($div.addClass('label'));
			}
			else { // (listType === "tiles")
				var label, _title;
				$input.attr({
					"type": _data.type || "radio",
					"name": _data.name,
					"value": _data.value
				}).addClass(_data.parent_input_type || "radio");
				label = (_data.summary_markup) ? _data.summary_markup : _data.label;
				_title = [_data.label];
				
                if (_data.disabled === true) {
                    $input.prop('disabled', true);
                    $label.addClass('disabled');
                    if (typeof(_data.disabled_status) === 'object' && _data.disabled_status.html_markup) { 
                        $label.attr('data-disabled-status', _data.disabled_status.html_markup);
                    }
                    else if (_data.disabled_status) {
                        $label.attr('data-disabled-status', _data.disabled_status);
                    }
                }
				if (_data.sections && _data.recurLevel != null) {
					$input.attr("data-choices-show-conditional", "true");
				}
				if (_data.details) {
					var _details = _data.details;
					if (_details.description) { $input.attr("title", _details.description); }

					if (_details.color) { _title.push('&#8212;', _details.color); }
					if (_details.brand) { _title.unshift(_details.brand); }
				}
				$span.append($span.clone().addClass('repos').append($input), label);
            
				$label.append($image);
				$label.append($span);
				$label.attr('title', _title.join(' '));
            
				$tile.append($label);
			}
                
            return $tile;
        },
        
        setUserChoice: function($src) {
            var _plugin = this;
            var $step_curr = $src.closest("fieldset");
            var _choices = {}, mode,
                index = $step_curr.data("choices_index"),
            	_choices_data = $step_curr.data("choices_data");
                
            if (!$step_curr.is("[data-choices-ref]") && $src.is(".btn")) { return; }
            
            var _options;
            if (_choices_data.choices) {
                _choices.name = $src.attr("name");
                _choices.field_name = _choices.name;
                _options = _choices_data.choices;
            }
            else if (_choices_data.listing_class === "fields") {
            	_choices.name = _choices_data.name;
            	_options = _choices_data.options;
            	mode = _choices_data.listing_class;
            }
            else {
            	_choices.name = _choices_data.name;
            	_choices.field_name = $step_curr.data("choices-ref");
            	_options = _choices_data.options;
            }

            $.each(_options, function (i, x) {
            	if (mode === "fields") {
            		_choices.field_name = x.name;
            		var $list = $step_curr.find(".fields");
            		$("li", $list).each(function (j, y) {
            			var $field = $(y).find("[name]");
            			var value = $field.val(),
							type = $field.is("[class]")? /(text|radio|checkbox)/.exec($field.attr("class"))[1] : ($field.attr("type") || $field.prop("tagName"));
            			_choices.value = value;
            		});
            		return true;
            	}
                else if (x.sections && index > 0) {
                    //console.log(i, x, _options, _choices, _choices_data);
                    
                }
            	else if (x.options_inset) {
            		var inset_values = $src.val().split("_");
            		if (inset_values[0] === x.value) {
            			_choices.user_option_index = i;
            			$.each(x.options_inset, function (j, y) {
                            if (!_choices.inset_name) { _choices.inset_name = y.name; }
            				if (inset_values[1] !== y.value) { return true; }
            				_choices.user_option_inset_index = j;
            			});
            		}
            		return true;
            	}
                if ($src.val() !== x.value) { return true; }
                _choices.user_option_index = i;
            })
            //console.log(_choices, _choices_data, index, $step_curr.data(), _options, _plugin._user_choice);
            var _user_choice = $.merge([], _plugin._user_choice || []);
            _user_choice.splice(index, _user_choice.length, _choices);
            _plugin._user_choice = _user_choice;
        },
        
        setNavCart: function() {
            var _plugin = this;
            var cartQuantity = _plugin.cartQuantity,
                $navCart = $("header a[href='#cart']");
            
            // if (!cartQuantity) {
//                 _plugin.updateCartPrices();
//             }
            
            $navCart.find("#nav-cart-qty").text(cartQuantity);
            if (cartQuantity > 0) {
            	$navCart.removeClass('disabled');
            	if (!$navCart.hasClass('enabled')) { $navCart.addClass('enabled'); }
            }
            else {
            	$navCart.addClass('disabled');
            	if ($navCart.hasClass('enabled')) { $navCart.removeClass('enabled'); }
            }
        },

        storeUserChoices: function() {
        	var _plugin = this;
        	
        	var _cart = $.merge([], _plugin.cart || []);
        	_cart.push({
                quantity: 1,
                user_choice: _plugin._user_choice
            });
        	_plugin.cart = _cart;
            _plugin._user_choice = null;
            if (!_plugin.cartQuantity) { 
                _plugin.cartQuantity = 1; 
            }
            else {
                _plugin.cartQuantity = _plugin.cartQuantity + 1;
            }
            _plugin.setNavCart();
            _plugin.resetChoices();
        },
        
        setOptionHeights: function() {
            var _plugin = this;
            var $page = (arguments.length && $(arguments[0]).length)? $(arguments[0]) : $(_plugin.$page_current),
                $steps = $page.find('fieldset');
        
            
            if (!$('html').hasClass('no-multiline-flexbox')) { return; }
            $steps.each(function(i, x) {
                if (!$(x).find('ul.tiles').length) { return true; }
                var $tiles = $(x).find('ul.tiles'),
                    heights = [];
                $.each($tiles.children('li'), function(j, y) {
                    var $tile = $(y).attr('data-height', '');
                    var height = $tile.outerHeight();
                        //$span = $tile.find('span').first();
                    //var span_height = $span.outerHeight();
                    //if ($span.is('[data-height]')) { $span.removeAttr('style'); }
                    //if ($.inArray(span_height, heights) === -1 && (!heights.length || span_height > heights[heights.length-1])) { heights.push(span_height); }
                    if ($.inArray(height, heights) === -1 && (!heights.length || height > heights[heights.length-1])) { heights.push(height); }
                });
                console.log(heights);
                //$tiles.find('li > label > span').css('height', 'auto');
                //if (heights.length > 1) { $tiles.find('li > label > span').attr('data-height', '').height(heights.pop()); }
                $tiles.find('li[data-height]').css('height', 'auto');
                if (heights.length > 1) { $tiles.find('li').height(heights.pop()); }
            });
            
        },
        
        setPage: function(mode) {
        	var _plugin = this;
        	var $page_curr = ($(_plugin.$page_current).length)? $(_plugin.$page_current) : $("section:visible");
        	var label;

        	switch (mode) {
        		case "add-another":
        			_plugin.setNextPage("home");
        			_plugin.resetChoices();
        			$('header #nav-menu li[data-choices-preset]').addClass('hidden');
        			label = true;
        			break;
        		case "add-confirm":
        			_plugin.storeUserChoices();
        			_plugin.setNextPage();
                    label = true;
        			break;
        		case "customise":
        			_plugin.setNextPage();
        			label = $("input:checked", $page_curr).closest("label").text();
        			$('header #nav-menu li.hidden').removeClass('hidden');
        			break;
                case "home":
                    _plugin.setNextPage("start");
                    break;
                case "order-confirm":
                    _plugin.setNextPage();
                    //_plugin.initValidation();
                    break;
                case "order-commit":
                    var $form = $(_plugin.element).find('form');
                    if (!$form.valid()) { return; }
                    _plugin.setNextPage();
                    
                    if ($._data($('main').get(0), 'events')['icon-image-props']) {
                        $('main').triggerHandler('icon-image-props', [_plugin.$page_next]);
                    }
                    _plugin.prepareCartSubmit(function () {
                        _plugin.cart = [];
                        _plugin.cartQuantity = 0;
                        _plugin.setNavCart();
                    });
                    break;
        		case "view-order":
        			_plugin.setNextPage("cart");
        			_plugin.loadCart();
        			label = true;
					break;
        		default:
        			_plugin.setNextPage();
        			break;
        	}
        	if ($page_curr.is("[id]") && $(_plugin.$page_next).is("[id]") && $page_curr.attr("id") === $(_plugin.$page_next).attr("id")) { return; }
        	if (label) {
        		_plugin.setHeader(label);
        	}
            _plugin.toNextPageFX();
        },
        
        prepareCartSubmit: function(callback) {
        	var _plugin = this;
            
        	$.getJSON(_plugin.options.dataSource, function (_data) {
                if (!_data.choices) { console.error("[Choices]: Invalid data source - expected 'choices' property."); return; }
                
                var $form = $(_plugin.element).find('form');
                
                var _submitDataAll = [];
                
                $.each(_plugin.cart, function(i, x) {
                    var _choice = _data.choices[x.user_choice[0].user_option_index];
                    var _submitData = {};
                    switch (x.user_choice[0].user_option_index) {
                        case 0: // Nutella
                            $.extend(true, _submitData, {
                                parent_product: _choice.name,
                                product: _choice.name,
                                product_summary: _choice.label_singular? _choice.label_singular : _choice.label,
                                quantity: x.quantity,
                                sku: _choice.details.sku,
                                price: parseFloat(_choice.details.price).toFixed(2),
                                personalisation: x.user_choice[1].value
                            });
                            break;
                        case 1: // Mr Men
                            var _character = _choice.sections[0].options[x.user_choice[1].user_option_index],
                                _frame = _choice.sections[1].options[x.user_choice[2].user_option_index],
                                _size = _choice.sections[1].options[x.user_choice[2].user_option_index].options_inset[x.user_choice[2].user_option_inset_index];
                            $.extend(true, _submitData, {
                                parent_product: _choice.name,
                                product: _character.value,
                                product_summary: [_character.label, '-', _frame.label, '(' + _size.label + ')'].join(' '),
                                quantity: x.quantity,
                                sku: _size.sku,
                                price: parseFloat(_size.price).toFixed(2),
                                personalisation: x.user_choice[3].value,
                                frame: _frame.value,
                                size: _size.value
                            });
                            break;
                        case 2: // Men's leather
                            var _type = _choice.sections[0].options[x.user_choice[1].user_option_index];
                            var _item = _type.sections[0].options[x.user_choice[2].user_option_index];
                            $.extend(true, _submitData, {
                                parent_product: _choice.name,
                                product: _item.value,
                                product_type: _type.value,
                                product_summary: [_item.details.brand, _item.label, _item.details.label, '-', _item.details.color, '(' + _item.details.description + ')'].join(' '),
                                quantity: x.quantity,
                                sku: _item.details.sku,
                                price: parseFloat(_item.details.price).toFixed(2),
                                personalisation: x.user_choice[3].value,
                                color: _item.details.color
                            });
                            break;
                        case 3: // Santa sack
                            var _type = _choice.sections[0].options[x.user_choice[1].user_option_index],
                                _color = _choice.sections[1].options[x.user_choice[2].user_option_index],
                                _personalisation_details = _choice.sections[2].content.personalisation;
                            $.extend(true, _submitData, {
                                parent_product: _choice.name,
                                product: _type.value,
                                product_summary: [_type.label, '-', _color.label, '(' + _type.details.description + ')'].join(' '),
                                quantity: x.quantity,
                                sku: _type.details.sku,
                                price: parseFloat(_type.details.price).toFixed(2),
                                personalisation: x.user_choice[3].value,
                                color: _color.value,
                                typeface: _personalisation_details.typeface
                            });
                            break;
                    }
                    _submitDataAll.push(_submitData);
                });
                
                $('form [name]').each(function(i, x) {
                    var $el = $(x);
                    if ($el.is(':checkbox') && $el.is("[data-submit-keep]")) { 
                        $('<input />').attr({
                            'type': 'hidden',
                            'name': $el.attr('name'),
                            'data-submit-keep': ''
                        }).val($el.is(":checked")? "yes": "no").insertAfter($el);
                        $el.removeAttr("data-submit-keep");
                    }
                    if ($el.is("[data-submit-keep]")) { return true; }
                    $el.attr('data-original-name', $(x).attr('name'));
                    $el.removeAttr('name');
                });
                
                $("[name='annotation_campaign_name']").val(_plugin.data.name);
                $("[name='annotation_address_delivery_different']").val($("[name='subscriber_custom_address_delivery_different']").val());
                
                var _date = new Date();
                var dateTodayRPN = [_date.getFullYear(), ('00' + (_date.getMonth()+1)).slice(-2), ('00' + _date.getDate()).slice(-2), '-', ('00' + _date.getHours()).slice(-2), ('00' + _date.getMinutes()).slice(-2)].join('')

                var $inputHidden = $('<input />').attr('type', 'hidden');

                $form.append($inputHidden.clone().attr('name', 'subscriber_custom_order_latest').val(dateTodayRPN));
                $form.append($inputHidden.clone().attr('name', 'subscriber_custom_order_' + dateTodayRPN + '_numitems').val(_plugin.cartQuantity));
                $form.append($inputHidden.clone().attr('name', 'subscriber_custom_order_' + dateTodayRPN + '_JSON_cart').val(JSON.stringify(_submitDataAll)));
                $form.append($inputHidden.clone().attr('name', 'subscriber_custom_order_' + dateTodayRPN + '_JSON__userchoice').val(JSON.stringify(_plugin.cart)));
                
                if (_plugin.allowSubmit) {
                	$.post($form.attr('action'), $form.serialize())
						.done(function (data) {
							console.info('[Choices]: submitted', data);
						})
						.fail(function () {
							console.error('[Choices]: Not submitted', arguments);
						})
						.always(function () {
							callback();
						});
                }
                else {
                	console.log('[Choices]: Submit skipped');
                	return callback();
                }
                
        	}).fail(function() {
        	    console.error('[Choices]: Data processing error - ', arguments);
        	});
            
            
        },

        setHeader: function(label) {
        	var _plugin = this,
				$breadcrumb = $("header .main .breadcrumb");

        	if (label === true) { label = _plugin.options.headerLabelDefault || "Personalised gifts"; }
        	if (!$breadcrumb.length || label.trim() === $breadcrumb.text().trim()) { return; }
        	$breadcrumb.fadeOut('fast', function () {
        		$(this).empty().append($('<li />').text(label)).fadeIn('fast');
        	});
        },
        
        updateCartPrices: function() {
        	var _plugin = this;
        	var $cart = $(_plugin.$page_current).find("#cart table"),
                _cart = _plugin.cart,
        		_data = _plugin.data.choices;
            var $cartList = $cart.find("tbody"),
                cartTotalPrice = 0,
                cartTotalQuantity = 0;
            
            if (_cart.length) {
                $.each(_cart, function(i, _x) {
                    var x = _x.user_choice;
                    var $cartListItem = $("tr[data-cart-item]", $cartList).eq(i);
                    $price = $cartListItem.find("[data-name='item-price']");
                
                    var _details = _data[x[0].user_option_index];
                
                    switch (x[0].user_option_index) {
                        case 0:
                            price = _details.details.price;
                            break;
                        case 1:
                            var _frame = _details.sections[1].options[x[2].user_option_index];
                            var _size = _frame.options_inset[x[2].user_option_inset_index];
                            price = _size.price;
                            break;
                        case 2:
                            var _type = _details.sections[0].options[x[1].user_option_index];
                            var _item = _type.sections[0].options[x[2].user_option_index];
                            price = _item.details.price;
                            break;
                        case 3:
                            var _type = _details.sections[0].options[x[1].user_option_index];
                            price = _type.details.price;
                            break;
                    }
                
                    quantityPrice = ((price && _x.quantity)? (parseInt(price) * parseInt(_x.quantity)) : 0);
                    cartTotalPrice += quantityPrice;
                    cartTotalQuantity+= _x.quantity;
                
                    $price.text(quantityPrice.toFixed(2));
                });
            }
            else {
                $cart.find('tbody').append($('<tr />').addClass('cart-empty').append($('<td />').attr("colspan", 3).append($('<div />').html("<h4>Your bag is empty!</h4>"))));
                
                $("a[href='#order-confirm']", $cart).addClass("disabled");
            }
            
            _plugin.cartQuantity = cartTotalQuantity;
            
            $("#cost-ex-deliv", $cart).text(cartTotalPrice.toFixed(2));
            
            var costDelivery = parseFloat($("#cost-deliv", $cart).text() || 9.95);
            if (cartTotalPrice > 100 || cartTotalPrice === 0) {
                costDelivery = 0;
                $('#cost-deliv', $cart).addClass('strikethrough');
            }
            else if ($('#cost-deliv.strikethrough', $cart).length) {
                $('#cost-deliv.strikethrough', $cart).removeClass('strikethrough');
            }
            var fullTotal = parseFloat(cartTotalPrice) + costDelivery;
            
            $("#cost-total", $cart).text(fullTotal.toFixed(2));            
            
            _plugin.setNavCart();
        },

        loadCart: function() {
            function elementEnclose(val, elType) {
                var className = (arguments.length >= 3)? arguments[2] : null,
                    $el = $(['<', elType, ' />'].join(''));
                if (className) { $el.addClass(className); }
                return (typeof(val) === 'object')? $.map(val, function(x, i) { return (typeof(x) === 'object' && x.length === 2)? $(['<', elType, ' />'].join('')).addClass(x[1]).append(x[0] || new String()).prop("outerHTML") : $el.clone().append(x || new String()).prop("outerHTML"); }) : $el.append(val || new String()).prop("outerHTML");
            }
            
        	var _plugin = this;
        	var $page_next = $(_plugin.$page_next);
        	var $cart = $page_next.find("#cart table"),
                _cart = $.merge([], _plugin.cart || []),
        		_data = _plugin.data.choices;
        	var $cartList = $cart.find("tbody"),
                cartTotalPrice = 0,
                cartTotalQuantity = 0;
            var $row = $('<tr />'), $col = $('<td />'), $div = $('<div />');

            var $row_spacer;
                
        	$cartList.empty();

            if (_cart.length) {
            	$.each(_cart, function (i, _x) {
                    var x = _x.user_choice;
                    var $dl = $('<dl />'), $dt = $('<dt />'), $dd = $('<dd />'), 
						$select = $('<select />').addClass('select'), 
						$input_text = $('<input />').attr({ 'type': 'text' }).addClass('text');
                    var $dl_view = $dl.clone(), 
						$dl_edit = $dl.clone();
            		var $col_desc = $col.clone(), $col_opt = $col.clone(), $col_price = $col.clone();
                
            		var _details = _data[x[0].user_option_index];
                    var _output = { 
                            item_type: _details.value, 
                            quantity: _x.quantity || 1 
                        };
                    
            		switch (x[0].user_option_index) {
            			case 0: // Nutella
                            var _personalisation = _details.sections[0].options[0],
                                _pers_details = _details.sections[0].content.personalisation;
                            $.extend(true, _output, {
                                label: elementEnclose([[_details.label_singular, _details.value + '_' + _details.name]], 'span'),
                                image: _details.image,
                                personalisation: x[1].value,
                                price: _details.details.price
                            });

            				// --------- VIEW ----------
            				// Name
            				$dl_view.append($dt.clone().html(_pers_details.inputType));
            				$dl_view.append($dd.clone().append($('<data />').attr('data-name', 'personalisation').text(_output.personalisation)));

        					// Quantity
            				$dl_view.append($dt.clone().html("Quantity"));
            				$dl_view.append($dd.clone().html($('<data />').attr('data-name', 'quantity').text(_output.quantity)));

            				// --------- EDIT----------
            				// Name
            				$dl_edit.append($dt.clone().html(_pers_details.inputType));
            				$dl_edit.append($dd.clone().append($input_text.clone().attr({
                                'name' : _pers_details.inputType,
                                'maxlength': _pers_details.maxLength
                            }).val(_output.personalisation).data('step-index', 1)));

            				// Quantity
            				$dl_edit.append($dt.clone().html("Quantity"));
            				var $select_qty = $select.clone().attr('name', 'quantity');
            				$.each(new Array(5), function (j, y) {
            					var $opt = $('<option />').text(j + 1);
            					if (_output.quantity === (j + 1)) { $opt.prop("selected", true); }
            					$select_qty.append($opt);
            				});
            				$dl_edit.append($dd.clone().append($select_qty));
            				break;
            			case 1: // Mr Men
                            var _character = _details.sections[0].options[x[1].user_option_index],
                                _frame = _details.sections[1].options[x[2].user_option_index],
                                _personalisation = _details.sections[2].options[0],
                                _pers_details = _details.sections[2].content.personalisation;
                            var _size = _frame.options_inset[x[2].user_option_inset_index];
                            $.extend(true, _output, {
                                label: elementEnclose([[_character.label + ' &#8211;', _character.name], [_frame.label, _frame.name]], 'span').join(''),
                                image: [_plugin.options.imagePath, '/thm_', _character.value, '_', _frame.value, '.jpg'].join(''),
                                personalisation: x[3].value,
                                price: _size.price,
                                size: _size.label
                            });

            				// --------- VIEW ----------
            				// Name
            				$dl_view.append($dt.clone().html(_pers_details.inputType));
            				$dl_view.append($dd.clone().append($('<data />').attr('data-name', 'personalisation').text(_output.personalisation)));

        					// Quantity
            				$dl_view.append($dt.clone().html("Quantity"));
            				$dl_view.append($dd.clone().html($('<data />').attr('data-name', 'quantity').text(_output.quantity)));

    						// Frame size
            				$dl_view.append($dt.clone().html("Size"));
                            $dl_view.append($dd.clone().html($('<data />').attr('data-name', 'size').text(_output.size)));

            				// --------- EDIT----------
            				// Name
            				$dl_edit.append($dt.clone().html(_pers_details.inputType));
            				$dl_edit.append($dd.clone().append($input_text.clone().attr({
                                'name' : _pers_details.inputType,
                                'maxlength': _pers_details.maxLength
                            }).val(_output.personalisation).data('step-index', 3)));

            				// Quantity
            				$dl_edit.append($dt.clone().html("Quantity"));
            				var $select_qty = $select.clone().attr('name', 'quantity');
            				$.each(new Array(5), function (j, y) {
            					var $opt = $('<option />').text(j + 1);
            					if (_output.quantity === (j + 1)) { $opt.prop("selected", true); }
            					$select_qty.append($opt);
            				});
            				$dl_edit.append($dd.clone().append($select_qty));

            				// Frame size
            				$dl_edit.append($dt.clone().html("Size"));
            				var $select_size = $select.clone().attr('name', 'size').data('step-index_inset', x[2].user_option_inset_index);
            				$.each(_frame.options_inset, function (j, y) {
            					var $opt = $('<option />').val(y.value).text(y.label);
            					if (_output.size === y.label) { $opt.prop("selected", true); }
            					$select_size.append($opt);
            				});
            				$dl_edit.append($dd.clone().append($select_size));
            				break;
            			case 2: // Men's Leather
                            var _type = _details.sections[0].options[x[1].user_option_index];
                            var _item = _type.sections[0].options[x[2].user_option_index],
                                _personalisation = _details.sections[1].options[0]
                                _pers_details = _details.sections[1].content.personalisation;
                            $.extend(true, _output, {
                                label: elementEnclose([[_item.details.brand, _item.name + '_brand'], [_item.label + ' &#8211;', _item.name + '_label'], [_item.details.color, _item.name + '_color']], 'span').join(''),
                                image: _item.image,
                                personalisation: x[3].value,
                                price: _item.details.price
                            });

            				// --------- VIEW ----------
            				// Name
            				$dl_view.append($dt.clone().html(_pers_details.inputType));
            				$dl_view.append($dd.clone().append($('<data />').attr('data-name', 'personalisation').text(_output.personalisation)));

        					// Quantity
            				$dl_view.append($dt.clone().html("Quantity"));
            				$dl_view.append($dd.clone().html($('<data />').attr('data-name', 'quantity').text(_output.quantity)));

            				// --------- EDIT----------
            				// Name
                            $dl_edit.append($dt.clone().html(_pers_details.inputType));
                            $dl_edit.append($dd.clone().append($input_text.clone().attr({
                                'name' : _pers_details.inputType,
                                'maxlength': _pers_details.maxLength
                            }).val(_output.personalisation).data('step-index', 3)));

            				// Quantity
                            $dl_edit.append($dt.clone().html("Quantity"));
                            var $select_qty = $select.clone().attr('name', 'quantity');
                            $.each(new Array(5), function (j, y) {
                            	var $opt = $('<option />').text(j + 1);
                            	if (_output.quantity === (j + 1)) { $opt.prop("selected", true); }
                            	$select_qty.append($opt);
                            });
                            $dl_edit.append($dd.clone().append($select_qty));
                            break;
            			case 3: // Santa sack
                            var _type = _details.sections[0].options[x[1].user_option_index],
                                _color = _details.sections[1].options[x[2].user_option_index],
                                _personalisation = _details.sections[2].options[0]
                                _pers_details = _details.sections[2].content.personalisation;
                            $.extend(true, _output, {
                                label: elementEnclose([[_type.label + ' &#8211;', _type.name], [_color.label, _type.name]], 'span'),
                                image: _type.image,
                                personalisation: x[3].value,
                                price: _type.details.price
                            });

            				// --------- VIEW ----------
            				// Name
            				$dl_view.append($dt.clone().html(_pers_details.inputType));
            				$dl_view.append($dd.clone().append($('<data />').attr('data-name', 'personalisation').text(_output.personalisation)));

        					// Quantity
            				$dl_view.append($dt.clone().html("Quantity"));
            				$dl_view.append($dd.clone().html($('<data />').attr('data-name', 'quantity').text(_output.quantity)));

            				// --------- EDIT----------
            				// Name
                            $dl_edit.append($dt.clone().html(_pers_details.inputType));
                            $dl_edit.append($dd.clone().append($input_text.clone().attr({
                                'name' : _pers_details.inputType,
                                'maxlength': _pers_details.maxLength
                            }).val(_output.personalisation).data('step-index', 3)));

            				// Quantity
                            $dl_edit.append($dt.clone().html("Quantity"));
                            var $select_qty = $select.clone().attr('name', 'quantity');
                            $.each(new Array(5), function (j, y) {
                            	var $opt = $('<option />').text(j + 1);
                            	if (_output.quantity === (j + 1)) { $opt.prop("selected", true); }
                            	$select_qty.append($opt);
                            });
                            $dl_edit.append($dd.clone().append($select_qty));
                            break;
            		}

                    if (Object.keys(_output).length) {
                		$col_desc.append($('<img />').attr('src', _output.image).addClass("image-" + _output.item_type));
                		$col_desc.append($div.clone().html(_output.label));

                		var $col_opt_wrapper = $div.clone().addClass("wrapper"),
							$col_opt_view = $div.clone().addClass("options-view"),
							$col_opt_edit = $div.clone().addClass("options-edit"),
                            $col_opt_link = $div.clone().addClass("col-control");

                		$col_opt_view.append($div.clone().addClass('col-content').append($dl_view), $col_opt_link.clone().append($('<a />').attr('href', '#change').text('Change details')));
                		$col_opt_edit.append($div.clone().addClass('col-content').append($dl_edit), $col_opt_link.clone().append($('<a />').attr('href', '#update').text('Update'), $('<a />').attr('href', '#cancel').text('Cancel')));

						$col_opt_wrapper.append($col_opt_view, $col_opt_edit);
                		$col_opt.append($col_opt_wrapper);
                    
                        _output.quantityPrice = ((_output.price && _output.quantity)? (_output.price * _output.quantity) : 0)
                        cartTotalPrice += _output.quantityPrice;

                        var $col_price_wrapper = $div.clone().addClass('wrapper'),
                            $price = $('<data />').attr('data-name', 'item-price').text(_output.quantityPrice.toFixed(2));
                        var $col_price_link = $div.clone().addClass("col-control").append($('<a />').attr('href', '#remove').text('Remove item'));
                    
                        $col_price_wrapper.append($div.clone().addClass('price-view').append($div.clone().addClass('col-content').append($price), $col_price_link));
                		$col_price.append($col_price_wrapper);
                        
                        cartTotalQuantity+= _output.quantity;
                    }
                
            		var $_row = $row.clone().attr('data-cart-item', '').append($col_desc, $col_opt, $col_price);
                
            		$cartList.append($_row, $row.clone().append($col.clone().addClass("spacer").attr("colspan", 3)));
                    
                    $("a[href='#order-confirm'].disabled", $cart).removeClass("disabled");
            	});
            }
            else {
                $row.append($col.attr("colspan", 3).addClass("cart-empty").append($div.html("<h4>Your bag is empty!</h4>")));
                
                $("a[href='#order-confirm']", $cart).addClass("disabled");
                
                $cartList.append($row, $row_spacer);
            }
            
            _plugin.cartQuantity = cartTotalQuantity;
            
            $("#cost-ex-deliv", $cart).text(cartTotalPrice.toFixed(2));
            
            var costDelivery = parseFloat($("#cost-deliv", $cart).text() || 9.95);
            
            if (cartTotalPrice > 100) {
                costDelivery = 0;
                $('#cost-deliv', $cart).addClass('strikethrough');
            }
            /*else if () {
                $('#cost-deliv.strikethrough', $cart).addClass('strikethrough')
            }
            // */
            
            var fullTotal = parseFloat(cartTotalPrice) + costDelivery;
            
            $("#cost-total", $cart).text(fullTotal.toFixed(2));
            
            _plugin.setNavCart();
            
            _plugin.initCartControls();
        },

        initCartControls: function() {
        	var _plugin = this;

        	$("#cart a[href='#change']").on('click', function (e) {
        		e.preventDefault();
        		var $src = $(e.currentTarget);
        		if (!$src.closest(".options-view").length) { return; }
        		var $panel = $src.closest('.options-view');
        		$panel.find('.col-control').fadeOut();
        		$panel.slideUp('fast');
        		$panel.siblings(".options-edit").slideDown('fast', function() {
        			$(this).find('.col-control').fadeIn();
        		});
        		$("#cart .control a[href='#order-confirm'].btn").addClass('disabled');
        	});
            
        	$("#cart a[href='#cancel']").on('click', function (e) {
        		e.preventDefault();
        		var $src = $(e.currentTarget);
        		if (!$src.closest(".options-edit").length) { return; }
        		var cart_item_index = $('#cart tbody tr[data-cart-item]').index($src.closest('tr')),
                    $panel = $src.closest('.options-edit');
                
        		if ($panel.find("[data-changed]").length) {
        			if (!confirm("Do you want to cancel your changes?")) { 
        				if (!$('#cart .options-edit:visible').length) {
        					$('#cart .control a.btn.disabled').removeClass('disabled');
        				}
        				return; 
        			}
        			$("[data-changed]", $panel).each(function(i, x) {
        				var $el = $(x);
                        
        				if ($el.is("[name='quantity']")) {
        					$el.val(_plugin.cart[cart_item_index].quantity.toString());
        				}
        				else if (Object.keys($el.data()).length || $el.is("[name='name'], [name='initials']")) {
        					var step_index = $el.data('step-index')? $el.data('step-index') : $el.data('step-index_inset')? $el.data('step-index_inset') : null;
        					if (step_index && $el.is(".text")) {
        						$el.val(_plugin.cart[cart_item_index].user_choice[step_index].value);
        					}
        					else if (step_index && $el.is(".select")) {
        						console.log($el.data(), step_index, $el.find('option').eq(step_index).text());
        						$el.find('option').eq(step_index).prop('selected', true);
        					}
        				}
        				$el.removeAttr("data-changed");
        			});
        		}
        		$panel.find('.col-control').fadeOut();
        		$panel.slideUp('fast');
        		$panel.siblings(".options-view").slideDown('fast', function() {
        			$(this).find('.col-control').fadeIn();
        			if (!$('#cart .options-edit:visible').length) {
        				$('#cart .control a.btn.disabled').removeClass('disabled');
        			}
        		});
        	});
            
        	$("#cart a[href='#update']").on('click', function(e) {
        		e.preventDefault();
        		var $src = $(e.currentTarget),
                    _cart = _plugin.cart;
        		if (!$src.closest(".options-edit").length) { return; }
        		var cart_item_index = $('#cart tbody tr[data-cart-item]').index($src.closest('tr'));
        		var _cart_item = _cart[cart_item_index];
        		var $panel = $src.closest('.options-edit');
        		var $panel_view = $panel.siblings('.options-view');
                
        		var _upd_user_choice = $.map(_cart_item.user_choice, function(x, i) {
        			var _x = x;
        			var $field = $('[name][data-changed]', $panel).filter("[name='" + _x.name + "'], [name='" + _x.inset_name + "']");
        			if (!$field.length && _x.name === 'personalisation') {
        				$field = $("[name='name'], [name='initials']", $panel);
        				_x.value = $field.val();
        				$panel_view.find("[data-name='personalisation']").text($field.val());
        				$field.removeAttr("data-changed");
        			}
        			else if (_x.inset_name && $field.prop("selectedIndex") != $field.data("stepIndex_inset")) {
        				_x.user_option_inset_index = $field.prop("selectedIndex");
        				$panel_view.find("[data-name='" + _x.inset_name + "']").text($field.val());
        				$field.data("stepIndex_inset", $field.prop("selectedIndex"));
        				$field.removeAttr("data-changed");
        			}
        			else if ($field.data("stepIndex") && $field.prop("selectedIndex") != $field.data("stepIndex")) {
        				_x.user_option_index = $field.prop("selectedIndex");
        				$panel_view.find("[data-name='" + _x.name + "']").text($field.val());
        				$field.data("stepIndex", $field.prop("selectedIndex"));
        				$field.removeAttr("data-changed");
        			}
        			return _x;
        		});
        		if ($("[name='quantity']", $panel).val() != _cart_item.quantity) {
        			var $field_qty = $("[name='quantity']", $panel);
        			_cart_item.quantity = parseInt($field_qty.val());
        			$panel_view.find("[data-name='quantity']").text($field_qty.val());
        			$field_qty.removeAttr("data-changed");
        		}
                else if ($("[name='size']", $panel).prop("selectedIndex") != _cart_item.user_choice[2].user_option_inset_index) {
                    var $field_size = $("[name='size']", $panel);
                    _cart_item.user_choice[2].user_option_inset_index = $field_size.prop("selectedIndex");
                    $panel_view.find("[data-name='size']").text($field_size.find('option').eq($field_size.prop('selectedIndex')).text());
                    $field_size.removeAttr('data-changed');
                }
                if ($("[name='name']", $panel).length) {
                    if (!$("[name='name']", $panel).val().length) {
                        $("[name='name']", $panel).addClass('error');
                        return;
                    }
                    else if ($("[name='name']", $panel).val().length &&  $("[name='name']", $panel).hasClass('error')) {
                        $("[name='name']", $panel).removeClass('error');
                    }
                }
                else if ($("[name='initials']")) {
                    if (!$("[name='initials']", $panel).val().length) {
                        $("[name='initials']", $panel).addClass('error');
                        return;
                    }
                    else if ($("[name='initials']", $panel).val().length &&  $("[name='initials']", $panel).hasClass('error')) {
                        $("[name='initials']", $panel).removeClass('error');
                    }
                }
        		_cart_item.user_choice = _upd_user_choice;
                
        		_cart[cart_item_index] = _cart_item;
        		_plugin.cart = _cart;
                
        		_plugin.updateCartPrices(); 
                
        		$panel.find('.col-control').fadeOut();
        		$panel.slideUp('fast');
        		$panel_view.slideDown('fast');
        		//if (!$('#cart .options-edit:visible').length) {
        		$panel_view.find('.col-control').fadeIn();
        		$('#cart .control a.btn.disabled').removeClass('disabled');
        		//}
        	});
            
        	$("#cart a[href='#remove']").on('click', function(e) {
        		e.preventDefault();
        		var $src = $(e.currentTarget),
                    _cart = _plugin.cart;
        		var cart_item_index = $('#cart tbody tr[data-cart-item]').index($src.closest('tr'));
                
        		if (!confirm("Are you sure you want to remove this item?")) { return; }
                
        		$src.closest("tr").next('tr').remove();
        		$src.closest("tr").remove();
        		_cart.splice(cart_item_index, 1);
        		_plugin.cart = _cart;

        		if (!$(_plugin.$page_current).find('[data-changed]').length && $("[href='#add-another']").is('.disabled')) {
        			$("[href='#add-another']").removeClass('disabled');
        		}
                
        		_plugin.updateCartPrices(); 
        	});
            
        	$("#cart input.text").on('keyup', $.debounce(100, function (e) {
        		var $src = $(e.currentTarget),
                    cart_item_index = $('#cart tbody tr[data-cart-item]').index($src.closest('tr'));
        		var pers_index = _plugin.cart[cart_item_index].user_choice.length-1
        		var _personalisation = _plugin.cart[cart_item_index].user_choice[pers_index];

        		if (_personalisation.value === $src.val()) { 
        			if ($src.is("[data-changed]")) { $src.removeAttr("data-changed"); }
        			return; 
        		}
        		$src.attr('data-changed', '');
        	}));
            
        	$("#cart select.select").on('change', function(e) {
        		var $src = $(e.currentTarget),
                    cart_item_index = $('#cart tbody tr[data-cart-item]').index($src.closest('tr'));
        		var step_index = ($src.data('step-index'))? $src.data('step-index') : $src.data('step-index_inset')? $src.data('step-index_inset') : null;
        		var _step = step_index? _plugin.cart[cart_item_index].user_choice[step_index] : null;
                
        		if (($src.is("[name='quantity']") && parseInt($src.val()) === _plugin.cart[cart_item_index].quantity) || (!$src.is("[name='quantity']") && (!step_index || !_step || _step.value === $src.val()))) {
        			if ($src.is("[data-changed]")) { $src.removeAttr("data-changed"); }
        			return;
        		}
                
        		$src.attr('data-changed', "");
        	});

        	$("#cart a[href='#add-another']").on('click', function (e) {
        		if ($('#cart').find('[data-changed]').length) {
					// ### TODO: Confirm/prompt add item when items still being updated
        		}
        	});
            
        },

        resetAll: function() {
        	var _plugin = this;

        	_plugin.resetChoices();
            _plugin.resetUserFields();
        	_plugin.toNextPageFX('entry');
        	if (_plugin.options.headerLabelDefault) { _plugin.setHeader(_plugin.options.headerLabelDefault); }
        },
        
        resetUserFields: function() {
            
        },

        resetChoices: function () {
        	var _plugin = this;

            _plugin._user_choice = null;
        	$("section[data-choices-ref='entry']", _plugin.$root).nextUntil("section[data-choices-ref='exit']").each(function(i, x) {
        		if (!$(x).is("[data-choices-ref='home']")) { $(x).removeClass('seen').addClass('unseen'); }
        		$("input:checked", x).prop("checked", false);
        		$("input.text", x).val("");
        		$("[data-choices-preset]", x).each(function (j, y) {
        			if ($(y).attr('class') === $(y).data('choices-preset')) { return true; }
        			$(y).addClass($(y).data("choices-preset"))
                    if ($(y).closest("#nav-menu").length) { $(y).hide(); }
        		});
        	});
        },

        setNextPage: function () {
        	var _plugin = this;
        	var _options = _plugin.options,
        		$page_next = (arguments.length && (new RegExp("^" + $.merge(Object.keys(_plugin.__defaults.pages), ["cart"]).join('|')+ "$")).test(arguments[0]))? $("section[data-choices-ref='" + arguments[0] + "']") : ($("section[id='" + arguments[0] + "']").length)? $('section#' + arguments[0]) : null;
        	var $page_curr = $(_plugin.$page_current);

        	if ($page_next) {
        		_plugin.$page_next = $page_next;
        		return;
        	}
        	if ($page_curr.is("section[data-choices-ref='entry']")) {
        		_plugin.$page_next = $("section[data-choices-ref='home']") || $page_curr.next('section');
        	}
        	else if ($page_curr.is("section[data-choices-ref='home']")) {
        		var $choice = $page_curr.find("input:checked");

        		_plugin.$page_next = $("section[data-choices-path='" + $choice.val() + "']");
        	}
        	else if ($page_curr.is("section[data-choices-path]")) {
        		_plugin.$page_next = $("section[data-choices-ref='exit']")
        	}
        	else if ($page_curr.is("section[data-choices-ref='exit'], section[data-choices-ref='cart'], section#details")) {
        		_plugin.$page_next = $page_curr.next('section');
        	}
        },

        toNextStepFX: function ($src) {
        	var _plugin = this;
        	var $page_curr = $(_plugin.$page_current);
        	var $step_curr = $src.closest("fieldset");
        	var $step_next = $step_curr.next('fieldset');
            
        	if ($src.is("[data-choices-show-conditional]")) {
        		var name = $src.attr("name"),
					value = $src.val();

        		var $step_show = $page_curr.find("fieldset[data-choices-show-condition='" + name + "=" + value + "']");

        		$step_show.siblings("fieldset[data-choices-show-condition^='" + name + "']:visible").fadeOut('fast').addClass("disabled").hide();
        		$step_show.hide().removeClass("disabled").fadeIn('slow');
        		$step_next = $step_show;
        		if ($step_next.length) {
        			$('html, body').animate({ scrollTop: ($step_next.offset().top - 50) }, 'swing');
        		}
        		//$step_show.nextAll("fieldset:not([data-choices-show-condition])").fadeIn('slow'); // UNCOMMENT AND REMOVE NEXT LINE TO ALLOW ALWAYS-VISIBLE STEPS ON PAGE
                $step_show.nextAll("fieldset:not([data-choices-show-condition])").hide();
                return;
        	}
        	
            // DISABLE FOLLOWING (IF) CONDITION TO ALLOW ALWAYS-VISIBLE STEPS ON PAGE
            if ($step_curr.is("[data-choices-show-condition]")) {
                var condition_name = $step_curr.attr("data-choices-show-condition").split("=")[0];
                $step_next = $step_curr.nextAll("fieldset:not([data-choices-show-condition^='" + condition_name + "'])").first().show().removeClass("disabled");
            }
            else {
        		//$step_next = $step_curr.nextAll('fieldset:visible').first().removeClass('disabled'); // UNCOMMENT AND REMOVE NEXT LINE TO ALLOW ALWAYS-VISIBLE STEPS ON PAGE
        		$step_next = $step_curr.nextAll('fieldset').first().show().removeClass('disabled');
            }

        	if (!$step_next.length) { return; }
    		$('html, body').animate({ scrollTop: ($step_next.offset().top - 50) }, 'swing', function() {
    		    //_plugin.setOptionHeights($page_next);
    		});
        },

        toNextPageFX: function () {
        	var _plugin = this;
        	var _userTriggers = $.extend(true, {}, _plugin.options.triggers);
        	var $page_curr = $(_plugin.$page_current),
            $page_next = $(_plugin.$page_next);
            
            if (arguments.length) {
                switch (typeof(arguments[0])) {
                    case "function":
                        $.extend(_userTriggers, { "animate-complete": arguments[0] });
                        break;
                    case "object":
                        $.extend(_userTriggers, arguments[0]);
                        break;
                }
            }
            
        	var currFxCss = { opacity: 0.0, height: 0 },
    			propName = new String(),
    			propUnit = new String(),
                isNextUnseen = $page_next.hasClass("unseen");

        	if ($(_plugin.element).hasClass('anim-vert')) {
        		propName = 'margin-top';
        		propUnit = 'vh';
        	}
        	else { // ($(_plugin.element).hasClass('anim-horz')) {
        		$(_plugin.element).addClass('anim-horz');
        		propName = 'margin-left';
        		propUnit = 'vw';
        	}

            var offset = 100; // pageNextVH
        	currFxCss[propName] = (((isNextUnseen) ? -1 : 1) * offset) + propUnit;
        	$page_curr.animate(currFxCss, {
        		start: function () {
        			$page_next.css({
        				'display': 'block',
        				'opacity': 0.0,
                        'scrollTop': 0
        			});

                    //_plugin.setOptionHeights($page_next);
                    
                    $('html, body').animate({ 'scrollTop': 0 }, 1); // FIX FOR IE & FIREFOX (html), IPAD (body)
                    
        			if (_userTriggers && _userTriggers['animate-start']) {
						_userTriggers['animate-start']();
					}
        		},
        		step: function (now, int) {
        			if (!/^v(w|h)$/.test(int.unit)) { return; }

        			propUnit = int.unit;

        			var nextPos = (isNextUnseen ? (100 - Math.abs(int.start + int.end) * int.pos) : (-100 + Math.abs(int.start + int.end) * int.pos));
        			var nextFxCss = {
        				'opacity': int.pos
                    };
        			nextFxCss[propName] = nextPos + propUnit;
        			$page_next.removeClass(isNextUnseen ? 'unseen' : 'seen').css(nextFxCss);

        			if (_userTriggers && _userTriggers['animate-step']) {
						_userTriggers['animate-step']();
					}
        		},
        		complete: function () {
        			$page_curr.hide().addClass(isNextUnseen ? 'seen' : 'unseen');
        			$page_curr.removeAttr("style");
                    
					if (_userTriggers && _userTriggers['animate-complete']) {
						_userTriggers['animate-complete']();
					}
        		},
        		always: function () {
        			_plugin.$page_current = $page_next;
        			_plugin.$page_next = null;
        		},
        		duration: 400,
        		easing: 'swing'
        	});

        }

    }

	$.fn[pluginName] = function() {
		var _args = arguments,
			options = _args[0];
		return this.each(function(i, x) {
			if (typeof(options) === "string") {
				switch(options) {
					case "cart":
					case "page":
                    case "reset":
					case "submit":
						var plugin = $.data(this, "plugin_" + pluginName);
                   		plugin[options].apply(plugin, Array.prototype.slice.call(_args, 1));
						break;
				}
                // */
			}
			else if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options));
			}
		});
	}
})(jQuery, window, document);
    