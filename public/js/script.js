$(function() {
    $('html').on({
        'set-useragent': function () {
            $('html').attr('user-agent', navigator.userAgent);
            var userAgent = $('html').attr('user-agent'), classes = new Array(), noFlexboxMultiline = false;
            if (!/(MSIE|Trident|rv:11|Edge|Firefox)/.test(userAgent)) { return; }
            if (/Firefox\/([\d.]+)/.test(userAgent)) { 
                var pattFf = /Firefox\/([\d][^.]+)[.]([\d]+)/.exec(userAgent);
                classes.push('ff_' + pattFf[1]); 
                if (parseInt(pattFf[1]) <= 28) { noFlexboxMultiline = true; }
            }
            else if (/Edge\/([\d]+)/.test(userAgent)) { classes.push('ie-edge' + /Edge\/([\d]+)/.exec(userAgent)[1]); }
            else if (/Trident.+(rv:11)/.test(userAgent)) { classes.push('ie11'); }
            else if (/MSIE (5|6|7|8|9|10)/.test(userAgent)) { classes.push('ie' + /MSIE (8|9|10)/.exec(userAgent)[1]); noFlexboxMultiline = true; }
            if (/MSIE (7|8|9)/.test(userAgent)) { classes.push('ie-legacy'); noFlexboxMultiline = true; }
            if (noFlexboxMultiline) { classes.push('no-multiline-flexbox'); }
            $('html').addClass(classes.join(' '));
        },
        'legacy-browser-notice': function () {
            $('body').addClass('browser-notice');
        },
        'browser-compat': function() {
            if ($('html').hasClass('ie9')) {
                $(window).on('load', function() {
                    // No form field placeholder support, so since labels are visible, change dropdown default text:
                    $('select.select').each(function(i, x) { $(x).children(':first').text('Select'); });
                });
            }
        }
    });

	$('main').on({
		'nav-bindings': function () {
            $('#nav-menu').on({
				'set-menu': function (e) {
					var $el = $(this);
					var $menu = $el.find('.menu');

					if ($menu.is(':visible')) {
						$menu.slideUp('fast');
						$el.removeClass('active');
						return;
					}
					$el.addClass('active');
					$menu.slideDown('fast');
				}
			});

            $('header #nav-menu a, header a.btn').on('click', function (e) {
            	e.preventDefault();
            	var $el = $(e.currentTarget);
                if ($el.is(".disabled")) { return; }
				//if ($el.is('#nav-menu')) {
				//  $(this).triggerHandler('set-menu');
				//}
				//else if ($el.is("a")) {
				$('#journey').choices('page', $el.attr('href'));
                //}
            	


            	
            });

			// ### TODO: FIX MENU HOVER AWAY
            //$('header #nav-menu').on({
            //	'mouseout': $.debounce(400, function (e) {
            //		var $btn = $(this).closest('#nav-menu');
            //		if (!$btn.hasClass('active')) { return; }
            //		$btn.triggerHandler('set-menu');
            //	})
			//}, '.menu');

            $(window).on('load resize', function (e) {
            	var $header = $('header'),
					$headerTitle = $("header .main");
            	var $breadcrumb = $(".breadcrumb li:first", $headerTitle),
            		breadcrumb_top = $breadcrumb.position().top;
            	if (!$breadcrumb.data('top') || breadcrumb_top > $breadcrumb.data('top')) {
            		$breadcrumb.data('top', breadcrumb_top);
            		if (breadcrumb_top > 15) {
						$breadcrumb.data('window-width', $(window).width());
            		}
            	}
            	if (breadcrumb_top > 15 || $(window).width() <= $breadcrumb.data('window-width')) {
            		$header.addClass('hide-title');
            	}
            	else if ($(window).width() > $breadcrumb.data('window-width')) {
					$header.removeClass('hide-title');
            	}
            });
        },
        
        'icon-image-props': function(e) {
			function resizeHeaderIcon($headerIcon) {
				var imageProps = $headerIcon.data("image-props");
				if (!imageProps || (imageProps.width && $headerIcon.width() > imageProps.width)) { return; }

				$headerIcon.height($headerIcon.width() / imageProps.ratio);
			}

			var $headerIcon = (arguments.length && $(arguments[1]).length)? $('.header h1 i', arguments[1]).first() : $('.header h1 i:visible').first();
			var imageProps = $headerIcon.data("image-props");
			if (!$headerIcon.length) { return; }

			if (!imageProps) {
				var imgSrc = /^url[(]["]{0,1}([^"]+)["]{0,1}[)]$/.exec($headerIcon.css('background-image'))[1];
				$("<img />").attr("src", imgSrc).load(function () {
					var width = $(this).prop("naturalWidth"),
						height = $(this).prop("naturalHeight");
					$headerIcon.data("image-props", {
						"src": $(this).prop("src"),
						"width": width,
						"height": height,
						"ratio": (width / height)
					});
					resizeHeaderIcon($headerIcon);
				});
				return;
			}
			resizeHeaderIcon($headerIcon);            
        },
        
		'heading-bindings': function () {
			$(window).on('load resize', function () {
				$('main').triggerHandler('icon-image-props');
			});
		},

		'field-bindings': function () {
			$('input.checkbox').on('change', function (e) {
				var $el = $(e.currentTarget);
				if (!$el.is("[data-show-context]") || !$($el.attr("data-show-context")).length) { return; }

				var $panel = $($el.attr("data-show-context"));

				if ($el.is(":checked") && !$panel.is(":visible")) {
					$panel.slideDown('fast');
				}
				else {
					$panel.slideUp('fast');
				}
			});
        }
	});

	$('html').triggerHandler('set-useragent');
	$('main').triggerHandler('field-bindings');
	$('main').triggerHandler('heading-bindings');
    $('html').triggerHandler('browser-compat');
    
    
	if (!$('section:visible').length) {
		$('section:first').show().animate({ opacity: 1.0 }, 200);
	}

    $('#journey').choices({ 
        dataSource: 'js/choices.json',
        debug: false,
        pages: {
            'entry': '#start',
            'home': '#item-select',
            'exit': '#item-added',
            'cart': '#order-summary'
        },
		homeName: 'item_type',
		animate: 'vert',
		headerLabelDefault: "Personalised gifts"
        /*,
        imagePath: '//myer.dtdmail.com.au/media/1/staging/persgifts/img'
        // */
    }).triggerHandler('nav-bindings');

	$.validator.addMethod('regex', function(val, el, re) {
		return (this.optional(el) || new RegExp(re).test(val));
	}, 'Please check input format');
    
    $("form").validate({
         ignore: ".ignore, :hidden",
         rules: {
            email: {
                email: true
            },
            subscriber_phone: {
                required: true,
                regex: /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/
            },
            subscriber_custom_delivery_address: {
                required: true
            },
            subscriber_custom_delivery_suburb: {
                required: true
            },
            subscriber_custom_delivery_postcode: {
                required: true,
                regex: /^(((2|8|9)\d{2})|((02|08|09)\d{2})|([1-9]\d{3}))$/
            },
            subscriber_address: {
                required: true
            },
            subscriber_suburb: {
                required: true
            },
            subscriber_postcode: {
                required: true,
                regex: /^(((2|8|9)\d{2})|((02|08|09)\d{2})|([1-9]\d{3}))$/
            }
           },
           onkeyup: false,
           errorPlacement: function(err, el) {
           }
    });
    
    $('form [name]').on('change', function(e) {
        var $el = $(e.currentTarget);
        if (!$el.val().length) { return; }
        $el.closest('.assist').removeClass('assist');
    })
    
});

